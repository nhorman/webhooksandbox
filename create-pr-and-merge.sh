#!/bin/sh

curl -s -o ./result.json -n https://api.bitbucket.org/2.0/repositories/nhorman/webhooksandbox/pullrequests \ -X POST --header 'Content-Type: application/json' --data @./data.json
MURL=$(jq '.links.merge.href' ./result.json | sed -e"s/\"//g")
echo "Sleeping for 10 seconds"
curl -s -n -o ./mergeresult.json -X POST --header 'Content-Type: application/json' --data @./merge.json $MURL
