FILENR:=$(shell dc -e "$$RANDOM 100 % p")
TESTFILE:=testfile$(FILENR).txt

debug:
	@echo ${FILENR}

test-merge:
	-git clean -f -d
	git checkout master
	git reset --hard
	-git branch -D test-merge-branch
	git pull origin master:master
	git checkout -b test-merge-branch
	-git rm -f *.txt
	echo "test file" > ./$(TESTFILE)
	git add ./$(TESTFILE) 
	git commit -m "test merge"
	git push origin test-merge-branch:test-merge-branch
	./create-pr-and-merge.sh
	git checkout master
	git branch -D test-merge-branch

.PHONY: test-merge debug

